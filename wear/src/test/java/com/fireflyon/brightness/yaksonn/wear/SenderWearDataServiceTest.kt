package com.fireflyon.brightness.yaksonn.wear

import com.fireflyon.birghtness.core.network.api.ApiService
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.utils.TABLET_BRIGHTNESS
import com.fireflyon.birghtness.core.utils.TABLET_SERIAL
import com.fireflyon.birghtness.core.utils.TIME
import com.fireflyon.birghtness.yaksonn.wear.service.SenderWearDataService
import com.fireflyon.birghtness.yaksonn.wear.ui.WearMainViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*
import kotlin.collections.HashMap


@ExperimentalCoroutinesApi
@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class SenderWearDataServiceTest {

    lateinit var service: SenderWearDataService

    @Mock
    lateinit var apiService: ApiService


    lateinit var apiRepository: ApiRepository

    lateinit var viewModel: WearMainViewModel

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        hiltRule.inject()
        service = SenderWearDataService()
        apiRepository = ApiRepository(apiService)
        viewModel = WearMainViewModel(apiRepository)
    }

    @Test
    fun serviceLogicTest() {
        //Given
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        TIME = date.toString()
        val brightnessParams = HashMap<String, String>()
        brightnessParams["serial"] = TABLET_SERIAL
        brightnessParams["time"] = TIME
        brightnessParams["brightness"] = TABLET_BRIGHTNESS

        //When
        service.onCreate()
        service.serviceLogic()
        verify(service, times(1)).senderBrightness(brightnessParams)
        verify(service, times(1)).initObservers()
    }
}