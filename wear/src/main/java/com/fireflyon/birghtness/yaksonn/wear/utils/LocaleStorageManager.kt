package com.fireflyon.birghtness.yaksonn.wear.utils

import android.content.Context
import android.content.SharedPreferences

object LocaleStorageManager {

    private lateinit var prefs: SharedPreferences
    private lateinit var prefsEditor: SharedPreferences.Editor

    private const val PREFS_NAME = "BRIGHTNESS_APP"

    fun init(context: Context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun setPreferences(prefName: String, prefValue: Boolean) {
        prefsEditor = prefs.edit()
        with(prefsEditor) {
            putBoolean(prefName, prefValue)
            commit()
        }
    }

    fun getPreferencesBoolVal(prefName: String): Boolean {
        return prefs.getBoolean(prefName, false)
    }
}