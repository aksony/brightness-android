package com.fireflyon.birghtness.yaksonn.wear

import android.app.Application
import com.fireflyon.birghtness.yaksonn.wear.utils.LocaleStorageManager
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WearApplicationClass : Application() {
    override fun onCreate() {
        super.onCreate()
        LocaleStorageManager.init(this)
    }
}