package com.fireflyon.birghtness.yaksonn.wear.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.fireflyon.birghtness.core.base.BaseViewModel
import com.fireflyon.birghtness.core.network.model.BrightnessModel
import com.fireflyon.birghtness.core.network.model.Status
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class WearMainViewModel @Inject constructor(
    private val apiRepository: ApiRepository,
) : BaseViewModel() {

    private val authenticate = MutableLiveData<Status<BrightnessModel>>()
    val authentication: LiveData<Status<BrightnessModel>> = authenticate

    private val brightness = MutableLiveData<Status<BrightnessModel>>()
    val brightnessData: LiveData<Status<BrightnessModel>> = brightness


    fun auth(paramsAuth: HashMap<String, String>) {
        viewModelScope.launch {
            apiRepository.authentication(paramsAuth = paramsAuth)
                .onStart { authenticate.value = loading() }
                .map { response -> fromResponse(response) }
                .collect { status -> authenticate.value = status }
        }
    }


    fun setBrightness(params: HashMap<String, String>) {
        viewModelScope.launch {
            apiRepository.setBrightness(params = params)
                .onStart { brightness.value = loading() }
                .map { response -> fromResponse(response) }
                .collect { status -> brightness.value = status }
        }
    }
}