package com.fireflyon.birghtness.yaksonn.wear.service

import android.util.Log
import com.fireflyon.birghtness.core.base.BaseSenderService
import com.fireflyon.birghtness.core.network.api.getApiService
import com.fireflyon.birghtness.core.network.model.Status
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.utils.*
import com.fireflyon.birghtness.yaksonn.wear.ui.WearMainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

@ExperimentalCoroutinesApi
class SenderWearDataService : BaseSenderService() {

    @Inject
    lateinit var wearMainViewModel: WearMainViewModel


    override fun onCreate() {
        super.onCreate()
        apiRepository = ApiRepository(apiService = getApiService())
        wearMainViewModel = WearMainViewModel(apiRepository = apiRepository)
    }

    override fun serviceLogic() {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        TIME = date.toString()
        Log.e("TAG_SERVICE", "Connect Service")
        val brightnessParams = HashMap<String, String>()
        brightnessParams["serial"] = WATCH_SERIAL
        brightnessParams["time"] = TIME
        brightnessParams["brightness"] = WATCH_BRIGHTNESS
        senderBrightness(paramsBrightness = brightnessParams)
        initObservers()
    }

    fun senderBrightness(paramsBrightness: HashMap<String, String>) =
        wearMainViewModel.setBrightness(params = paramsBrightness)

    fun initObservers() {
        wearMainViewModel.brightnessData.observe(this) { state ->
            when (state) {
                is Status.Loading -> {

                }
                is Status.Success -> {
                    toast("Success")
                }
                is Status.Error -> {
                    toast(state.message)
                }
            }
        }
    }
}