package com.fireflyon.birghtness.core.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


abstract class BaseActivity<VM : ViewModel, VB : ViewDataBinding> : AppCompatActivity() {

    lateinit var binding: VB

    /**
     * Returns Current ViewModel Instance
     */
    val viewModel: VM by lazy { ViewModelProvider(this).get(getViewModelKey()) }

    /**
     * Layout resource id
     */
    abstract fun getLayoutId(): Int

    /**
     * Returns ViewModel class type
     */
    protected abstract fun getViewModelKey(): Class<VM>

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        binding.lifecycleOwner = this
        prepareView(savedInstanceState)
    }

    /**
     * Prepare UI Components here
     */
    abstract fun prepareView(savedInstanceState: Bundle?)

}