package com.fireflyon.birghtness.core.network.repository

import com.fireflyon.birghtness.core.network.api.ApiService
import com.fireflyon.birghtness.core.network.model.BrightnessModel
import com.fireflyon.birghtness.core.network.model.GenericResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject


@ExperimentalCoroutinesApi
class ApiRepository @Inject constructor(
    private val apiService: ApiService
) : ApiRepositoryImpl {

    override suspend fun authentication(paramsAuth: HashMap<String, String>): Flow<GenericResponse<BrightnessModel>> {
        return object : NetworkBoundRepository<BrightnessModel, BrightnessModel>() {
            override suspend fun fetchFromRemote(): Response<BrightnessModel> {
                return apiService.authApp(paramsAuth = paramsAuth)
            }
        }.asFlow()
    }


    override suspend fun setBrightness(params: HashMap<String, String>): Flow<GenericResponse<BrightnessModel>> {
        return object : NetworkBoundRepository<BrightnessModel, BrightnessModel>() {
            override suspend fun fetchFromRemote(): Response<BrightnessModel> {
                return apiService.setBrightness(paramsBrightness = params)
            }
        }.asFlow()
    }

}