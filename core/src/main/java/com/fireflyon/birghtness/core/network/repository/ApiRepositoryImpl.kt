package com.fireflyon.birghtness.core.network.repository

import com.fireflyon.birghtness.core.network.model.BrightnessModel
import com.fireflyon.birghtness.core.network.model.GenericResponse
import kotlinx.coroutines.flow.Flow

interface ApiRepositoryImpl {

    suspend fun authentication(paramsAuth: HashMap<String, String>) : Flow<GenericResponse<BrightnessModel>>

    suspend fun setBrightness(params: HashMap<String, String>) : Flow<GenericResponse<BrightnessModel>>
}