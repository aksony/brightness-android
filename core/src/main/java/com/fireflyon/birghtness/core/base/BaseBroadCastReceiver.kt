package com.fireflyon.birghtness.core.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

abstract class BaseBroadCastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.let {
            processBroadcast(context = context, intent = it)
        }
    }

    abstract fun processBroadcast(context: Context?, intent: Intent)
}