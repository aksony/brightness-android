package com.fireflyon.birghtness.core.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BrightnessModel(
    val token: String,
    val time: String,
    val serial: String,
    val brightnessRatio: Double
) : Parcelable
