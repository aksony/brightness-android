package com.fireflyon.birghtness.core.network.model

sealed class Status<T>{

    class Loading<T> : Status<T>()

    data class Success<T>(val data: T) : Status<T>()

    data class Error<T>(val message: String) : Status<T>()

    fun isLoading(): Boolean = this is Loading

    fun isSuccessful(): Boolean = this is Success

    fun isFailed(): Boolean = this is Error

}
