package com.fireflyon.birghtness.core.base


import androidx.lifecycle.ViewModel
import com.fireflyon.birghtness.core.network.model.GenericResponse
import com.fireflyon.birghtness.core.network.model.Status

abstract class BaseViewModel : ViewModel() {


    /**
     * Returns [Status.Loading] instance.
     */
    fun <T> loading() = Status.Loading<T>()

    /**
     * Returns [Status.Success] instance.
     * @param data Data to emit with status.
     */
    private fun <T> success(data: T) = Status.Success(data)

    /**
     * Returns [Status.Error] instance.
     * @param message Description of failure.
     */
    private fun <T> error(message: String) = Status.Error<T>(message)

    /**
     * Returns [Status] from [GenericResponse]
     */
    fun <T> fromResponse(resource: GenericResponse<T>): Status<T> = when (resource) {
        is GenericResponse.Success -> success(resource.data)
        is GenericResponse.Failed -> error(resource.message)

    }
}