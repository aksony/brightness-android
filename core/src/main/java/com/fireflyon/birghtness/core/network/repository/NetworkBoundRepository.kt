package com.fireflyon.birghtness.core.network.repository

import android.util.Log
import androidx.annotation.MainThread
import com.fireflyon.birghtness.core.network.model.GenericResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import retrofit2.Response


@ExperimentalCoroutinesApi
abstract class NetworkBoundRepository<RESULT, REQUEST> {

    fun asFlow() = flow<GenericResponse<RESULT>> {

        // Fetch latest data from remote
        val apiResponse = fetchFromRemote()

        // Parse body
        val remoteData = apiResponse.body()

        // Check for response validation
        if (apiResponse.isSuccessful && remoteData != null) {
            Log.d("TAG","Success")
        } else {
            // Something went wrong! Emit Error state.
            emit(GenericResponse.Failed(apiResponse.message()))
        }

    }.catch { e ->
        e.printStackTrace()
        emit(GenericResponse.Failed("Network error! Please try again."))
    }


    /**
     * Fetches [GenericResponse] from the remote end point.
     */
    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<REQUEST>
}