package com.fireflyon.birghtness.core.network.api

import com.fireflyon.birghtness.core.BuildConfig
import com.fireflyon.birghtness.core.network.api.ApiService
import com.fireflyon.birghtness.core.utils.authToken
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


private val interceptor = Interceptor {
    val request = it.request()
    val requestBuilder = request.newBuilder()
        .addHeader("Authorization", authToken ?: "")

    return@Interceptor it.proceed(requestBuilder.build())
}

private val interceptorContent = Interceptor {
    val request = it.request()
    val requestBuilder = request.newBuilder()
        .addHeader("Content-Type", "application/x-www-form-urlencoded")

    return@Interceptor it.proceed(requestBuilder.build())
}

private fun getClient(): OkHttpClient.Builder {
    return OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .addNetworkInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE
        })
        .apply {
            if (authToken.isBlank())
                addInterceptor(interceptorContent)
            else
                addInterceptor(interceptor)
        }
}

val moshi: Moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

fun getRetrofit(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BuildConfig.API_BASE_URL)
        .client(getClient().build())
        .build()
}

fun getApiService(): ApiService {
    return getRetrofit().create(ApiService::class.java)
}


