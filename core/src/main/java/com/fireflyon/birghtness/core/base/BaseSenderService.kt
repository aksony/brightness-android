package com.fireflyon.birghtness.core.base

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Intent
import android.os.IBinder
import android.os.SystemClock
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.ViewModelProvider
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

abstract class BaseSenderService : LifecycleService() {

    @ExperimentalCoroutinesApi
    @Inject
    lateinit var apiRepository: ApiRepository

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        onTaskRemoved(intent)
        serviceLogic()
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        serviceLogic()
        return super.onBind(intent)
    }

    override fun startService(service: Intent?): ComponentName? {
        return super.startService(service)
    }

    override fun startForegroundService(service: Intent?): ComponentName? {
        return super.startForegroundService(service)
    }

    //call service periodically
    override fun onTaskRemoved(rootIntent: Intent?) {

        val restartService = Intent(
            applicationContext,
            this.javaClass
        )
        restartService.setPackage(packageName)
        val restartServicePI = PendingIntent.getService(
            applicationContext, 1, restartService,
            PendingIntent.FLAG_ONE_SHOT
        )
        val alarmService = applicationContext.getSystemService(ALARM_SERVICE) as AlarmManager
        alarmService[AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 60000] =
            restartServicePI

        super.onTaskRemoved(rootIntent)
    }

    abstract fun serviceLogic()

}