package com.fireflyon.birghtness.core.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


fun saveData(context: Context, mJsonData: String?) {
    val fileName: String = " config.json"
    try {
        val obj = JSONObject()
        val subObj = JSONObject()
        subObj.put("device_type", mJsonData)
        obj.put("Content", subObj)
        val sdCard = Environment.getExternalStorageDirectory()
        val dir = File(sdCard.absolutePath)
        val file = File(dir, fileName)
        val fileOutputStream: FileOutputStream = FileOutputStream(file)
        fileOutputStream.write(obj.toString().toByteArray())
        fileOutputStream.close()
        Log.e("TAG", "JSONNN: $obj")
        context.toast("File created successfully")
    } catch (e: IOException) {
        Log.e("TAG", "Error Result: " + e.localizedMessage)
    }
}

fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun checkPermissionFile(context: Context): Boolean {
    return (ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) != PackageManager.PERMISSION_GRANTED) &&
            (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED)

}


fun requestPermissionsFile(permission: Int, activity: Activity) {
    ActivityCompat.requestPermissions(
        activity,
        arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ),
        permission
    )
}


