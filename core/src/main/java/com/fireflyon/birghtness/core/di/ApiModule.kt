package com.fireflyon.birghtness.core.di

import com.fireflyon.birghtness.core.BuildConfig
import com.fireflyon.birghtness.core.network.api.ApiService
import com.fireflyon.birghtness.core.network.api.DynamicInterceptor
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.Interceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideRetrofitService(): ApiService = Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .addConverterFactory(
            MoshiConverterFactory.create(Moshi.Builder().add(KotlinJsonAdapterFactory()).build())
        ).build().create(ApiService::class.java)

    @ExperimentalCoroutinesApi
    @Singleton
    @Provides
    fun providesRepository(apiService: ApiService) = ApiRepository(apiService)

    @Singleton
    @Provides
    fun provideInterceptor(): Interceptor = DynamicInterceptor()

//    @Singleton


}