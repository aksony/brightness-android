package com.fireflyon.birghtness.core.network.model

sealed class GenericResponse<T> {
    class Success<T>(val data: T) : GenericResponse<T>()
    class Failed<T>(val message: String) : GenericResponse<T>()
}
