package com.fireflyon.birghtness.core.utils

const val REQUEST_WRITE_FILE_TO_STORAGE = 1234

const val DEVICE_TABLET = "tablet"
const val DEVICE_TV = "television"
const val DEVICE_WEAR = "watch"

const val TABLET_SERIAL = "TBxAD2"
const val TV_SERIAL = "TVxAD3"
const val WATCH_SERIAL = "WTxAD1"


var TABLET_BRIGHTNESS = ""
var TV_BRIGHTNESS = ""
var WATCH_BRIGHTNESS = ""

var TIME = ""

var authToken = ""

var IS_SAVED_TABLET = "IS_SAVED_TABLET"
var IS_SAVED_TV = "IS_SAVED_TV"
var IS_SAVED_WATCH = "IS_SAVED_WATCH"


