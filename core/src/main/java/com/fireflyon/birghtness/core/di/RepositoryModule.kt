package com.fireflyon.birghtness.core.di

import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.network.repository.ApiRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class RepositoryModule {

    @ActivityRetainedScoped
    @Binds
    abstract fun bindApiRepository(repository: ApiRepository): ApiRepositoryImpl
}