package com.fireflyon.birghtness.core.network.api

import android.util.Log
import com.fireflyon.birghtness.core.utils.authToken
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response

class DynamicInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        if (authToken.isNotBlank()){
            requestBuilder.addHeader("Authorization", authToken)
        }

        return chain.proceed(requestBuilder.build()).also {
            Log.d("Request Body ", Gson().toJson(it.request.body))
        }
    }
}