package com.fireflyon.birghtness.core.network.api

import com.fireflyon.birghtness.core.network.model.BrightnessModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface ApiService {

    @POST("auth")
    suspend fun authApp(@Body paramsAuth: Map<String, String>?): Response<BrightnessModel>

    @POST("brightness")
    suspend fun setBrightness(@Body paramsBrightness: Map<String, String>?): Response<BrightnessModel>

}