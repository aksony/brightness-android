package com.fireflyon.brightness.yaksonn.service

import android.content.Context
import android.content.Intent
import android.os.Build
import com.fireflyon.birghtness.core.base.BaseBroadCastReceiver
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SenderDataBroadCastReceiver : BaseBroadCastReceiver() {

    override fun processBroadcast(context: Context?, intent: Intent) {
        val i = Intent(context, SenderDataService::class.java)
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            context?.startForegroundService(i)
        }else{
            context?.startService(i)
        }
    }
}