package com.fireflyon.brightness.yaksonn.ui

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.fireflyon.birghtness.core.base.BaseActivity
import com.fireflyon.birghtness.core.network.model.Status
import com.fireflyon.birghtness.core.utils.*
import com.fireflyon.brightness.yaksonn.R
import com.fireflyon.brightness.yaksonn.databinding.ActivityMobileMainBinding
import com.fireflyon.brightness.yaksonn.service.SenderDataBroadCastReceiver
import com.fireflyon.brightness.yaksonn.utils.LocaleStorageManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import kotlin.collections.HashMap


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : BaseActivity<MobileMainViewModel, ActivityMobileMainBinding>() {

    override fun getViewModelKey(): Class<MobileMainViewModel> {
        return MobileMainViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_mobile_main
    }

    override fun prepareView(savedInstanceState: Bundle?) {
        initViews()
    }

    fun initViews() {
        val brightnessValue: Int = Settings.System.getInt(
            contentResolver, Settings.System.SCREEN_BRIGHTNESS, 0
        )
        binding.run {
            brightnessCountTextView.text = brightnessValue.toString()
        }
        TABLET_BRIGHTNESS = brightnessValue.toString()


        if (checkPermissionFile(this@MainActivity)) {
            requestPermissionsFile(REQUEST_WRITE_FILE_TO_STORAGE, this@MainActivity)
        } else {
            if (!LocaleStorageManager.getPreferencesBoolVal(IS_SAVED_TABLET)) {
                saveData(this@MainActivity, DEVICE_TABLET)
                LocaleStorageManager.setPreferences(IS_SAVED_TABLET, true)
            }
        }

        val serialDevice = HashMap<String, String>()
        serialDevice["serial"] = TABLET_SERIAL
        auth(serialDevice)

        startSendBrightnessToService()

        initObservers()
    }

    /**
     * Initialize Observers
     */
    private fun initObservers() {
        viewModel.authentication.observe(this) { state ->
            when (state) {
                is Status.Loading -> {

                }
                is Status.Success -> {
                    toast("Auth Success")
                    authToken = state.data.token
                }
                is Status.Error -> {
                    toast(state.message)
                }
            }
        }
    }

    private fun auth(paramsAuth: HashMap<String, String>) = viewModel.auth(paramsAuth = paramsAuth)

    @SuppressLint("ShortAlarm")
    private fun startSendBrightnessToService() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val myIntent = Intent(this, SenderDataBroadCastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, myIntent, 0)

        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.add(Calendar.SECOND, 60)

        val frequency = (60 * 1000).toLong()

        alarmManager
            .setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                frequency,
                pendingIntent
            )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_WRITE_FILE_TO_STORAGE -> {
                saveData(this@MainActivity,DEVICE_TABLET)
                LocaleStorageManager.setPreferences(IS_SAVED_TABLET, true)
            }
        }
    }

}