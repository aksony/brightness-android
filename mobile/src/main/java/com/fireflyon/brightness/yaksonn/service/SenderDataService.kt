package com.fireflyon.brightness.yaksonn.service

import android.util.Log
import com.fireflyon.birghtness.core.base.BaseSenderService
import com.fireflyon.birghtness.core.network.api.getApiService
import com.fireflyon.birghtness.core.network.model.Status
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.utils.TABLET_BRIGHTNESS
import com.fireflyon.birghtness.core.utils.TABLET_SERIAL
import com.fireflyon.birghtness.core.utils.TIME
import com.fireflyon.birghtness.core.utils.toast
import com.fireflyon.brightness.yaksonn.ui.MobileMainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


@ExperimentalCoroutinesApi
class SenderDataService : BaseSenderService() {

    @Inject
    lateinit var mobileMainViewModel: MobileMainViewModel

    override fun onCreate() {
        super.onCreate()
        apiRepository = ApiRepository(apiService = getApiService())
        mobileMainViewModel = MobileMainViewModel(apiRepository = apiRepository)
    }

    override fun serviceLogic() {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        TIME = date.toString()
        Log.e("TAG_SERVICE", "Connect Service")
        val brightnessParams = HashMap<String, String>()
        brightnessParams["serial"] = TABLET_SERIAL
        brightnessParams["time"] = TIME
        brightnessParams["brightness"] = TABLET_BRIGHTNESS
        senderBrightness(paramsBrightness = brightnessParams)
        initObservers()
    }

    fun senderBrightness(paramsBrightness: HashMap<String, String>) =
        mobileMainViewModel.setBrightness(paramsBrightness = paramsBrightness)

    fun initObservers() {
        mobileMainViewModel.brightnessData.observe(this) { state ->
            when (state) {
                is Status.Loading -> {

                }
                is Status.Success -> {
                    toast("Success")
                }
                is Status.Error -> {
                    toast(state.message)
                }
            }
        }
    }

}