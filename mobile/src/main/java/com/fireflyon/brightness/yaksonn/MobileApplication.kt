package com.fireflyon.brightness.yaksonn

import android.app.Application
import com.fireflyon.brightness.yaksonn.utils.LocaleStorageManager
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MobileApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        LocaleStorageManager.init(this)
    }
}