package com.fireflyon.brightness.yaksonn

import com.fireflyon.birghtness.core.network.api.ApiService
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.utils.TABLET_BRIGHTNESS
import com.fireflyon.birghtness.core.utils.TABLET_SERIAL
import com.fireflyon.birghtness.core.utils.TIME
import com.fireflyon.brightness.yaksonn.service.SenderDataService
import com.fireflyon.brightness.yaksonn.ui.MobileMainViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*
import kotlin.collections.HashMap


@ExperimentalCoroutinesApi
@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class SenderDataServiceTest {

    lateinit var service: SenderDataService

    @Mock
    lateinit var apiService: ApiService


    lateinit var apiRepository: ApiRepository

    lateinit var viewModel: MobileMainViewModel

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        hiltRule.inject()
        service = SenderDataService()
        apiRepository = ApiRepository(apiService)
        viewModel = MobileMainViewModel(apiRepository)
    }

    @Test
    fun serviceLogicTest() {
        //Given
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        TIME = date.toString()
        val brightnessParams = HashMap<String, String>()
        brightnessParams["serial"] = TABLET_SERIAL
        brightnessParams["time"] = TIME
        brightnessParams["brightness"] = TABLET_BRIGHTNESS

        //When
        service.onCreate()
        service.serviceLogic()
        verify(service, times(1)).senderBrightness(brightnessParams)
        verify(service, times(1)).initObservers()
    }
}