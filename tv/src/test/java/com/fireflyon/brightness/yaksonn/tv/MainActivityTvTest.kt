package com.fireflyon.brightness.yaksonn.tv

import android.content.ContentResolver
import android.provider.Settings
import com.fireflyon.birghtness.yaksonn.tv.R
import com.fireflyon.birghtness.yaksonn.tv.ui.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config


@ExperimentalCoroutinesApi
@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class MainActivityTvTest {

    private var contentResolver: ContentResolver? = null

    lateinit var activity: MainActivity

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        hiltRule.inject()
        activity = MainActivity()
        contentResolver = RuntimeEnvironment.application.contentResolver
    }

    @Test
    fun getLayoutIdTest() {
        //When
        activity.getLayoutId()

        //Then
        assertEquals(R.layout.activity_tv_main, activity.getLayoutId())
    }


    @Test
    fun initViewsTest() {

        //Given
        val brightnessValue: Int = Settings.System.getInt(
            contentResolver, Settings.System.SCREEN_BRIGHTNESS, 0
        )
        //When
        `when`(activity.contentResolver).thenReturn(contentResolver)
        activity.prepareView(null)
        activity.initViews()

        //Then
        assertEquals(activity.binding.brightnessCountTextView.text, brightnessValue.toString())

    }

}