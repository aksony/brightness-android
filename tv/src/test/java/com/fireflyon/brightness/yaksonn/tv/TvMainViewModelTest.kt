package com.fireflyon.brightness.yaksonn.tv

import com.fireflyon.birghtness.core.network.model.BrightnessModel
import com.fireflyon.birghtness.core.network.repository.ApiRepository
import com.fireflyon.birghtness.core.utils.*
import com.fireflyon.birghtness.yaksonn.tv.ui.TvMainViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.robolectric.annotation.Config
import java.util.*
import kotlin.collections.HashMap
import org.robolectric.RobolectricTestRunner


@ExperimentalCoroutinesApi
@HiltAndroidTest
@Config(application = HiltTestApplication::class)
@RunWith(RobolectricTestRunner::class)
class TvMainViewModelTest {

    private val job = Job()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(job + testDispatcher)

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

//    @InjectMocks
//    lateinit var apiService: ApiService

    @Mock
    lateinit var apiRepository: ApiRepository

    lateinit var viewModel: TvMainViewModel

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        hiltRule.inject()
        viewModel = TvMainViewModel(apiRepository)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun verifyAuth() = testScope.runBlockingTest {
        //Given
        val hashMap = HashMap<String, String>()
        hashMap["serial"] = "TEST_XDEIA"
        val brightnessModel = BrightnessModel("3712973", "", "TEST_XDEIA", 0.0)

        // When
        viewModel.auth(hashMap)

        //Then
        assertNotNull(viewModel.authentication.value)
//        assertEquals(viewModel.authentication.value, brightnessModel.token)

        verify(apiRepository, times(1)).authentication(hashMap)
    }

    @Test
    fun verifyBrightness() = testScope.runBlockingTest {
        //Given
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        TIME = date.toString()

        val brightnessParams = HashMap<String, String>()

        brightnessParams["serial"] = TABLET_SERIAL
        brightnessParams["time"] = TIME
        brightnessParams["brightness"] = TABLET_BRIGHTNESS

        val brightnessModel = BrightnessModel("3712973", TIME, TABLET_SERIAL, 0.0)


        //When
        viewModel.setBrightness(brightnessParams)

        //Then
        assertNotNull(viewModel.brightnessData.value)
        assertEquals(viewModel.brightnessData.value, brightnessModel)

        verify(apiRepository, times(1)).setBrightness(brightnessParams)

    }


}