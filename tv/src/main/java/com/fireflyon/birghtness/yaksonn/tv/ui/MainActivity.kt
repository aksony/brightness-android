package com.fireflyon.birghtness.yaksonn.tv.ui

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.fireflyon.birghtness.core.base.BaseActivity
import com.fireflyon.birghtness.core.network.model.Status
import com.fireflyon.birghtness.core.utils.*
import com.fireflyon.birghtness.yaksonn.tv.R
import com.fireflyon.birghtness.yaksonn.tv.databinding.ActivityTvMainBinding
import com.fireflyon.birghtness.yaksonn.tv.service.SenderTvDataBroadCastReceiver
import com.fireflyon.birghtness.yaksonn.tv.utils.LocaleStorageManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : BaseActivity<TvMainViewModel, ActivityTvMainBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_tv_main
    }

    override fun getViewModelKey(): Class<TvMainViewModel> {
        return TvMainViewModel::class.java
    }

    override fun prepareView(savedInstanceState: Bundle?) {
        initViews()
    }

    fun initViews() {
        val brightnessValue: Int = Settings.System.getInt(
            contentResolver, Settings.System.SCREEN_BRIGHTNESS, 0
        )
        binding.brightnessCountTextView.text = brightnessValue.toString()

        TV_BRIGHTNESS = brightnessValue.toString()

        if (checkPermissionFile(this@MainActivity)) {
            requestPermissionsFile(REQUEST_WRITE_FILE_TO_STORAGE, this@MainActivity)
        } else {
            if (!LocaleStorageManager.getPreferencesBoolVal(IS_SAVED_TV)) {
                saveData(this@MainActivity,DEVICE_TV)
                LocaleStorageManager.setPreferences(IS_SAVED_TV, true)
            }
        }

        val serialDevice = HashMap<String, String>()
        serialDevice["serial"] = TV_SERIAL
        auth(serialDevice)

        startSendBrightnessToService()

        initObservers()
    }

    /**
     * Initialize Observers
     */
    private fun initObservers() {
        viewModel.authentication.observe(this) { state ->
            when (state) {
                is Status.Loading -> {

                }
                is Status.Success -> {
                    toast("Auth Success")
                    authToken = state.data.token
                }
                is Status.Error -> {
                    toast(state.message)
                }
            }
        }
    }

    private fun auth(paramsAuth: HashMap<String, String>) = viewModel.auth(paramsAuth = paramsAuth)

    private fun startSendBrightnessToService() {

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val myIntent = Intent(this, SenderTvDataBroadCastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, myIntent, 0)

        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.add(Calendar.SECOND, 60)
        val date: Date = calendar.time
        TIME = date.toString()
        val frequency = (60 * 1000).toLong()

        alarmManager
            .setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                frequency,
                pendingIntent
            )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_WRITE_FILE_TO_STORAGE -> {
                saveData(this@MainActivity,DEVICE_TABLET)
                LocaleStorageManager.setPreferences(IS_SAVED_TV, true)
            }
        }
    }

}