package com.fireflyon.birghtness.yaksonn.tv

import android.app.Application
import com.fireflyon.birghtness.yaksonn.tv.utils.LocaleStorageManager
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TvApplicationClass : Application() {
    override fun onCreate() {
        super.onCreate()
        LocaleStorageManager.init(this)
    }
}